package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.Address;
import com.deltahaze.straitscommerce.Repositories.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AddressServicesImpl implements AddressServices {

    @Autowired
    private AddressRepository address_repo;

    @Override
    public Address saveAddress(Address Address) {
        return address_repo.save(Address);
    }

    @Override
    public Address getAddressById(int id) {
        return address_repo.findById(id).orElse(null);
    }

    @Override
    public void deleteAddress(int id) {
        Address user = address_repo.findById(id).orElse(null);
        address_repo.delete(user);
    }

    @Override
    public List<Address> getAllAddresses() {
        return address_repo.findAll();
    }
}


