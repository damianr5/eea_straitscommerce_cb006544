package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.Supplier;

import java.util.List;


public interface SupplierServices {

    Supplier saveSupplier(Supplier Supplier);

    Supplier getSupplierById(int id);

    void deleteSupplier(int id);

    List<Supplier> getAllSuppliers();
}
