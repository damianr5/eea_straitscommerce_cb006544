package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.Orders;
import com.deltahaze.straitscommerce.Models.User;

import java.util.List;


public interface OrderServices {

    Orders saveOrder(Orders Order);

    List<Orders> getOrdersById(User user);

    void deleteOrder(int id);

    List<Orders> getAllOrders();
}
