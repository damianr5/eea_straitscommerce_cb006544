package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.Product;

import java.util.List;


public interface ProductServices {

    Product saveProduct(Product Product);

    Product getProductById(int id);

    void deleteProduct(int id);

    List<Product> getAllProducts();
}
