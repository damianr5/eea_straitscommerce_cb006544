package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.Supplier;
import com.deltahaze.straitscommerce.Repositories.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SupplierServicesImpl implements SupplierServices {

    @Autowired
    private SupplierRepository supplier_repo;

    @Override
    public Supplier saveSupplier(Supplier Supplier) {
        return supplier_repo.save(Supplier);
    }

    @Override
    public Supplier getSupplierById(int id) {
        return supplier_repo.findById(id).orElse(null);
    }

    @Override
    public void deleteSupplier(int id) {
        Supplier user = supplier_repo.findById(id).orElse(null);
        supplier_repo.delete(user);
    }

    @Override
    public List<Supplier> getAllSuppliers() {
        return supplier_repo.findAll();
    }
}


