package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.Product;
import com.deltahaze.straitscommerce.Repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProductServicesImpl implements ProductServices {

    @Autowired
    private ProductRepository product_repo;

    @Override
    public Product saveProduct(Product Product) {
        return product_repo.save(Product);
    }

    @Override
    public Product getProductById(int id) {
        return product_repo.findById(id).orElse(null);
    }

    @Override
    public void deleteProduct(int id) {
        Product user = product_repo.findById(id).orElse(null);
        product_repo.delete(user);
    }

    @Override
    public List<Product> getAllProducts() {
        return product_repo.findAll();
    }
}


