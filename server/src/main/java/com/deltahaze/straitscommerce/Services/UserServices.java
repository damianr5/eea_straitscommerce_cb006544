package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.User;

import java.util.List;


public interface UserServices {

    User saveUser(User User);

    User getUserById(int id);

    User getUserByLoginInformation(String username, String password);

    void deleteUser(int id);

    List<User> getAllUsers();

    User updateUser(int id, User user);
}
