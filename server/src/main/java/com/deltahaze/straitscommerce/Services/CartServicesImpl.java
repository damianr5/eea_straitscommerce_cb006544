package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.Cart;
import com.deltahaze.straitscommerce.Models.Product;
import com.deltahaze.straitscommerce.Models.User;
import com.deltahaze.straitscommerce.Repositories.CartRepository;
import com.deltahaze.straitscommerce.Repositories.ProductRepository;
import com.deltahaze.straitscommerce.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class CartServicesImpl implements CartServices {

    @Autowired
    private CartRepository cart_repo;

    @Autowired
    private UserRepository user_repo;

    @Autowired
    private ProductRepository product_repo;

    @Override
    public Cart saveCart(Cart Cart) {
        Optional<User> optionalUser = user_repo.findById(Cart.getUser().getId());
        Optional<Product> optionalProduct = product_repo.findById(Cart.getProducts().get(0).getId());

        if (optionalUser.isPresent() && optionalProduct.isPresent()){
            Cart cart = cart_repo.findCartByUser(optionalUser.get());
            if (cart==null){
                cart = new Cart();
            }

            cart.setUser(optionalUser.get());

            boolean status = true;
            List<Product> productList = cart.getProducts();
            if (productList!=null){
                for (Product product : productList){
                    if (product.getId().equals(optionalProduct.get().getId())){
                        status = false;
                        break;
                    }
                }
            }

            if (status){
                if (productList==null){
                    productList = new ArrayList<>();
                }
                productList.add(optionalProduct.get());
                cart.setProducts(productList);
            }
            return cart_repo.save(cart);
        }
        return null;
    }

    @Override
    public Cart getCartById(int id) {
        Optional<User> optionalUser = user_repo.findById(id);
        if(optionalUser.isPresent()){
            Cart cart = cart_repo.findCartByUser(optionalUser.get());
            return cart;
        }
        return null;
    }

    @Override
    public void deleteCart(int id) {
        Cart user = cart_repo.findById(id).orElse(null);
        cart_repo.delete(user);
    }

    @Override
    public List<Cart> getAllCarts() {
        return cart_repo.findAll();
    }
}


