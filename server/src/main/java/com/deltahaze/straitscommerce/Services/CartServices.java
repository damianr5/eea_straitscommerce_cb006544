package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.Cart;

import java.util.List;


public interface CartServices {

    Cart saveCart(Cart Cart);

    Cart getCartById(int id);

    void deleteCart(int id);

    List<Cart> getAllCarts();
}
