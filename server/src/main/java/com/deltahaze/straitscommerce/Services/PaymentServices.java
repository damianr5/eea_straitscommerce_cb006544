package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.Payment;

import java.util.List;


public interface PaymentServices {

    Payment savePayment(Payment Payment);

    Payment getPaymentById(int id);

    void deletePayment(int id);

    List<Payment> getAllPayments();
}
