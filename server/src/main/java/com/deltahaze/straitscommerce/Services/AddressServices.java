package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.Address;

import java.util.List;


public interface AddressServices {

    Address saveAddress(Address Address);

    Address getAddressById(int id);

    void deleteAddress(int id);

    List<Address> getAllAddresses();
}
