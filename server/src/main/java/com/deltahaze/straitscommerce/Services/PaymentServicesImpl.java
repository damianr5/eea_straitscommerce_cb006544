package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.Payment;
import com.deltahaze.straitscommerce.Repositories.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PaymentServicesImpl implements PaymentServices {

    @Autowired
    private PaymentRepository payment_repo;

    @Override
    public Payment savePayment(Payment Payment) {
        return payment_repo.save(Payment);
    }

    @Override
    public Payment getPaymentById(int id) {
        return payment_repo.findById(id).orElse(null);
    }

    @Override
    public void deletePayment(int id) {
        Payment user = payment_repo.findById(id).orElse(null);
        payment_repo.delete(user);
    }

    @Override
    public List<Payment> getAllPayments() {
        return payment_repo.findAll();
    }
}


