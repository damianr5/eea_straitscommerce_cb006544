package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.Orders;
import com.deltahaze.straitscommerce.Models.User;
import com.deltahaze.straitscommerce.Repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class OrderServicesImpl implements OrderServices {

    @Autowired
    private OrderRepository order_repo;

    @Override
    public Orders saveOrder(Orders Order) {
        return order_repo.save(Order);
    }

    @Override
    public List<Orders> getOrdersById(User user) {

        return order_repo.findOrdersByUser(user);
    }

    @Override
    public void deleteOrder(int id) {
        Orders user = order_repo.findById(id).orElse(null);
        order_repo.delete(user);
    }

    @Override
    public List<Orders> getAllOrders() {
        return order_repo.findAll();
    }
}


