package com.deltahaze.straitscommerce.Services;

import com.deltahaze.straitscommerce.Models.User;
import com.deltahaze.straitscommerce.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserServicesImpl implements UserServices {

    @Autowired
    private UserRepository user_repo;


    @Override
    public User saveUser(User User) {
        return user_repo.save(User);
    }

    @Override
    public User getUserById(int id) {
        return user_repo.findById(id).orElse(null);
    }

    @Override
    public User getUserByLoginInformation(String username, String password) {
        return user_repo.findUserByUsernameAndPassword(username, password);
    }

    @Override
    public User updateUser(int id, User newUser){
        Optional<User> optionalUser = user_repo.findById(id);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            user.setFirst_name(newUser.getFirst_name());
            user.setLast_name(newUser.getLast_name());
            user.setEmail(newUser.getEmail());
            user.setDob(newUser.getDob());
            user.setUsername(newUser.getUsername());
            user.setPassword(newUser.getPassword());

            return user_repo.save(user);
        }
        return null;
    }

    @Override
    public void deleteUser(int id) {
        User user = user_repo.findById(id).orElse(null);
        user_repo.delete(user);
    }

    @Override
    public List<User> getAllUsers() {
        return user_repo.findAll();
    }
}


