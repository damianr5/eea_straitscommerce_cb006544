package com.deltahaze.straitscommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StraitscommerceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StraitscommerceApplication.class, args);
	}

}
