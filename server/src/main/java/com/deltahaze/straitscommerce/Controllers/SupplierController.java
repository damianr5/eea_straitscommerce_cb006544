package com.deltahaze.straitscommerce.Controllers;

import com.deltahaze.straitscommerce.Models.Supplier;
import com.deltahaze.straitscommerce.Services.SupplierServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/supplier")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SupplierController {

    @Autowired
    private SupplierServices SupplierService;

    @GetMapping
    public List<Supplier> getAllSuppliers() {
        return SupplierService.getAllSuppliers();
    }

    @GetMapping("/{id}")
    public Supplier getSupplier(@PathVariable("id") int id) {
        return SupplierService.getSupplierById(id);
    }

    @PostMapping
    public Supplier createSupplier(@RequestBody Supplier supplier) {
        return SupplierService.saveSupplier(supplier);
    }

    @PutMapping
    public Supplier updateSupplier(@RequestBody Supplier supplier) {
        return SupplierService.saveSupplier(supplier);
    }

    @DeleteMapping("/{id}")
    public void deleteSupplier(@PathVariable("id") int id) {
        SupplierService.deleteSupplier(id);
    }
}
