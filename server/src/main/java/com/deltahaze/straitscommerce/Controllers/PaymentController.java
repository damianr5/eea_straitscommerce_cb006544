package com.deltahaze.straitscommerce.Controllers;

import com.deltahaze.straitscommerce.Models.Payment;
import com.deltahaze.straitscommerce.Services.PaymentServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/payment")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PaymentController {

    @Autowired
    private PaymentServices PaymentService;

    @GetMapping
    public List<Payment> getAllPayments() {
        return PaymentService.getAllPayments();
    }

    @GetMapping("/{id}")
    public Payment getPayment(@PathVariable("id") int id) {
        return PaymentService.getPaymentById(id);
    }

    @PostMapping
    public Payment createPayment(@RequestBody Payment payment) {
        return PaymentService.savePayment(payment);
    }

    @PutMapping
    public Payment updatePayment(@RequestBody Payment payment) {
        return PaymentService.savePayment(payment);
    }

    @DeleteMapping("/{id}")
    public void deletePayment(@PathVariable("id") int id) {
        PaymentService.deletePayment(id);
    }
}
