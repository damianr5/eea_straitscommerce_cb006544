package com.deltahaze.straitscommerce.Controllers;

import com.deltahaze.straitscommerce.Models.User;
import com.deltahaze.straitscommerce.Services.UserServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/adminRegistration")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RegisterAdminController {

        @Autowired
        private UserServices userServices;

        @PostMapping
        public User createUser(@RequestBody User user){
            return userServices.saveUser(user);
        }
    }

