package com.deltahaze.straitscommerce.Controllers;

import com.deltahaze.straitscommerce.Models.Product;
import com.deltahaze.straitscommerce.Services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProductController {

    @Autowired
    private ProductServices ProductService;

    @GetMapping
    public List<Product> getProducts() {
        return ProductService.getAllProducts();
    }

    @GetMapping("/{id}")
    public Product getProduct(@PathVariable("id") int id) {
        return ProductService.getProductById(id);
    }


    @PostMapping
    public Product createProduct(@RequestBody Product product) {
        return ProductService.saveProduct(product);
    }

    @PutMapping
    public Product updateProduct(@RequestBody Product product) {
        return ProductService.saveProduct(product);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable("id") int id) {
        ProductService.deleteProduct(id);
    }
}
