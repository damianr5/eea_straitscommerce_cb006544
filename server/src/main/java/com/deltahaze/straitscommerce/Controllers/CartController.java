package com.deltahaze.straitscommerce.Controllers;
import com.deltahaze.straitscommerce.Models.Cart;
import com.deltahaze.straitscommerce.Services.CartServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cart")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CartController {

    @Autowired
    private CartServices CartService;

    @GetMapping
    public List<Cart> getAllCarts() {
        return CartService.getAllCarts();
    }

    @GetMapping("/{id}")
    public Cart getCart(@PathVariable("id") int id) {
        return CartService.getCartById(id);
    }

    @PostMapping
    public Cart createCart(@RequestBody Cart cart) {
        System.out.print("cart" +cart.getUser());
        System.out.println("cart" +cart.getProducts().get(0).getId());
        return CartService.saveCart(cart);

    }

    @PutMapping
    public Cart updateCart(@RequestBody Cart cart) {
        return CartService.saveCart(cart);
    }

    @DeleteMapping("/{id}")
    public void deleteCart(@PathVariable("id") int id) {
        CartService.deleteCart(id);
    }
}
