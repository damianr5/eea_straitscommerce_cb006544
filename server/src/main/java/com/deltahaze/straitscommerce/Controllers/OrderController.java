package com.deltahaze.straitscommerce.Controllers;

import com.deltahaze.straitscommerce.Models.*;
import com.deltahaze.straitscommerce.Services.*;
import dto.AddressDTO;
import dto.CartDTO;
import dto.OrdersDTO;
import dto.PaymentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/orders")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class OrderController {

    @Autowired
    private OrderServices OrderService;

    @Autowired
    private AddressServices addressServices;

    @Autowired
    private PaymentServices paymentServices;

    @Autowired
    private CartServices cartServices;

    @Autowired
    private UserServices userServices;

    @GetMapping
    public List<OrdersDTO> getAllOrders() {
        List<Orders> ordersList = OrderService.getAllOrders();
        List<OrdersDTO> orderDTOList = new ArrayList<>();
        for(Orders orders : ordersList){
            orderDTOList.add(ordersToDTO(orders));
        }
        return orderDTOList;
    }

//    @GetMapping("/{id}")
//    public Orders getOrder(@PathVariable("id") int id) {
//        return OrderService.getOrderById(id);
//    }

    @PostMapping
    public OrdersDTO createOrder(@RequestBody Orders order) {

        System.out.println("order" +order.getAddresses());
        Address address = order.getAddresses();
        address = addressServices.saveAddress(address);

        Payment payment = order.getPayment();
        payment = paymentServices.savePayment(payment);


        Cart cart = cartServices.getCartById(order.getCart().getUser().getId());
        order.setAddresses(address);
        order.setPayment(payment);
        order.setCart(cart);

        return ordersToDTO(OrderService.saveOrder(order));

    }

    @PutMapping
    public Orders updateOrder(@RequestBody Orders order) {
        return OrderService.saveOrder(order);
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(@PathVariable("id") int id) {
        OrderService.deleteOrder(id);
    }

    private OrdersDTO ordersToDTO(Orders order){
        OrdersDTO ordersDTO = new OrdersDTO();
        ordersDTO.setId(order.getId());
        ordersDTO.setOrder_date(order.getOrder_date());
        ordersDTO.setTotal_price(order.getTotal_price());

        ordersDTO.setAddresses(addressToDTO(order.getAddresses()));
        ordersDTO.setPayment(paymentToDTO(order.getPayment()));
        ordersDTO.setCart(cartToDTO(order.getCart()));
        return ordersDTO;
    }

    private CartDTO cartToDTO(Cart cart){
        CartDTO cartDTO = new CartDTO();

        cartDTO.setCart_total(cart.getCart_total());
        cartDTO.setQuantity(cart.getQuantity());
        cartDTO.setUser(cart.getUser());

        return cartDTO;
    }

    private PaymentDTO paymentToDTO(Payment payment){
        PaymentDTO paymentDTO = new PaymentDTO();

        paymentDTO.setPayment_type(payment.getPayment_type());
        paymentDTO.setCard_type(payment.getCard_type());
        paymentDTO.setCard_name(payment.getCard_name());
        paymentDTO.setCard_number(payment.getCard_number());
        paymentDTO.setCard_expiry_date(payment.getCard_expiry_date());
        paymentDTO.setCvv(payment.getCvv());

        return paymentDTO;
    }

    private AddressDTO addressToDTO(Address address){
        AddressDTO addressDTO = new AddressDTO();

        addressDTO.setStreet1(address.getStreet1());
        addressDTO.setStreet2(address.getStreet2());
        addressDTO.setCity(address.getCity());
        addressDTO.setCountry(address.getCountry());
        addressDTO.setZip_code(address.getZip_code());

        return addressDTO;
    }
}
