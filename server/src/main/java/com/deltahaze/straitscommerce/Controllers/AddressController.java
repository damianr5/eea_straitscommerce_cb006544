package com.deltahaze.straitscommerce.Controllers;

import com.deltahaze.straitscommerce.Models.Address;
import com.deltahaze.straitscommerce.Services.AddressServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/address")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AddressController {

    @Autowired
    private AddressServices AddressService;

    @GetMapping
    public List<Address> getAllAddresses() {
        return AddressService.getAllAddresses();
    }

    @GetMapping("/{id}")
    public Address getAddress(@PathVariable("id") int id) {
        return AddressService.getAddressById(id);
    }

    @PostMapping
    public Address createAddress(@RequestBody Address address) {
        return AddressService.saveAddress(address);
    }

    @PutMapping
    public Address updateAddress(@RequestBody Address address) {
        return AddressService.saveAddress(address);
    }

    @DeleteMapping("/{id}")
    public void deleteAddress(@PathVariable("id") int id) {
        AddressService.deleteAddress(id);
    }
}
