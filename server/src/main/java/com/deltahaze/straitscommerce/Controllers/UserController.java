package com.deltahaze.straitscommerce.Controllers;

import com.deltahaze.straitscommerce.Models.User;
import com.deltahaze.straitscommerce.Services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

    @Autowired
    private UserServices UserService;

    @GetMapping
    public List<User> getAllUsers() {
        return UserService.getAllUsers();
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable("id") int id) {
        return UserService.getUserById(id);
    }

    @PostMapping
    public User createUser(@RequestParam Map<String, String> body) {
        User user = new User();
        user.setFirst_name(body.get("firstName"));
        user.setLast_name(body.get("lastName"));
        user.setUsername(body.get("username"));
        user.setEmail(body.get("email"));
        user.setPassword(body.get("password"));
        user.setDob(body.get("dob"));
        System.out.println("user" + user);
        return UserService.saveUser(user);
    }

    @PutMapping("/{id}")
    public User updateUser(@RequestParam Map<String, String> body, @PathVariable int id) {

        String first_name = body.get("first_name");
        String last_name = body.get("last_name");
        String username = body.get("username");
        String email = body.get("email");
        String password = body.get("password");
        String dob = body.get("dob");

        User user = new User();
        user.setFirst_name(first_name);
        user.setLast_name(last_name);
        user.setEmail(email);
        user.setDob(dob);
        user.setUsername(username);
        user.setPassword(password);
        return UserService.updateUser(id, user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") int id) {
        UserService.deleteUser(id);
    }
}
