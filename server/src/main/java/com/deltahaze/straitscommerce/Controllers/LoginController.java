package com.deltahaze.straitscommerce.Controllers;

import com.deltahaze.straitscommerce.Models.User;
import com.deltahaze.straitscommerce.Services.UserServices;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/login")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LoginController {

    @Autowired
    private UserServices userServices;

    @PostMapping
    public ResponseEntity<String> userAuthorisation(@RequestParam Map<String, String> body){
        String username = body.get("username");
        String password = body.get("password");

        if(username.isEmpty()|| password.isEmpty()){
            return new ResponseEntity<>("Username or password is empty!", HttpStatus.NOT_FOUND);
        }
        else{
            User user = userServices.getUserByLoginInformation(username, password);
            System.out.print("user" +user);
            ObjectMapper objectmapper = new ObjectMapper();
            String userId = "";
            if(user!=null){
                try {
                    userId = objectmapper.writeValueAsString(user.getId());
                    return new ResponseEntity<>(userId, HttpStatus.OK);
            }catch(JsonProcessingException e)
                {
                    e.printStackTrace();
                }

            }
            return new ResponseEntity<>("Username or password is invalid!", HttpStatus.NOT_FOUND);
        }
    }
}
