package com.deltahaze.straitscommerce.Repositories;

import com.deltahaze.straitscommerce.Models.Address;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AddressRepository extends JpaRepository<Address, Integer> {


}
