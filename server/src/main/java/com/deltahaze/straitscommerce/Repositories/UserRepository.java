package com.deltahaze.straitscommerce.Repositories;

import com.deltahaze.straitscommerce.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Integer> {

    User findUserByUsernameAndPassword(String username, String password);

}
