package com.deltahaze.straitscommerce.Repositories;

import com.deltahaze.straitscommerce.Models.Orders;
import com.deltahaze.straitscommerce.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface OrderRepository extends JpaRepository<Orders, Integer> {

    List<Orders> findOrdersByUser(User user);
}
