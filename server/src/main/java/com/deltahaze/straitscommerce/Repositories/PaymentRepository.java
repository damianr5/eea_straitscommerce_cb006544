package com.deltahaze.straitscommerce.Repositories;

import com.deltahaze.straitscommerce.Models.Payment;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PaymentRepository extends JpaRepository<Payment, Integer> {


}
