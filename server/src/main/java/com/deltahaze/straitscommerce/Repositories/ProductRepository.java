package com.deltahaze.straitscommerce.Repositories;

import com.deltahaze.straitscommerce.Models.Product;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductRepository extends JpaRepository<Product, Integer> {


}
