package com.deltahaze.straitscommerce.Repositories;

import com.deltahaze.straitscommerce.Models.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SupplierRepository extends JpaRepository<Supplier, Integer> {


}
