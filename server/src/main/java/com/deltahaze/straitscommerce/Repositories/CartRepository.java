package com.deltahaze.straitscommerce.Repositories;

import com.deltahaze.straitscommerce.Models.Cart;
import com.deltahaze.straitscommerce.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CartRepository extends JpaRepository<Cart, Integer> {
    Cart findCartByUser(User user);

}
