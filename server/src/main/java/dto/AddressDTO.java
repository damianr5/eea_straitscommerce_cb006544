package dto;

import com.deltahaze.straitscommerce.Models.Orders;
import com.deltahaze.straitscommerce.Models.Product;
import com.deltahaze.straitscommerce.Models.Supplier;
import com.deltahaze.straitscommerce.Models.User;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AddressDTO {
    private Integer id;
    private String street1;
    private String street2;
    private String city;
    private String zip_code;
    private String country;

    private List<User> users;

    private Supplier supplier;

    private List<Orders> orders;

    private List<Product> products;

    public AddressDTO() {
    }
}
