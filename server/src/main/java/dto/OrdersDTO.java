package dto;

import com.deltahaze.straitscommerce.Models.Address;
import com.deltahaze.straitscommerce.Models.Cart;
import com.deltahaze.straitscommerce.Models.Payment;
import com.deltahaze.straitscommerce.Models.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrdersDTO {
    Integer id;
    private int no_of_items;
    private double total_price;
    private String order_date;

    private CartDTO cart;

    private User user;

    private PaymentDTO payment;

    private AddressDTO addresses;

    public OrdersDTO(){

    }

}
