package dto;

import com.deltahaze.straitscommerce.Models.Orders;
import com.deltahaze.straitscommerce.Models.Product;
import com.deltahaze.straitscommerce.Models.User;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CartDTO {
    private Integer id;
    private int quantity;
    private double cart_total;

    private User user;

    private Orders order;

    private List<Product> products;

    public CartDTO() {
    }
}
