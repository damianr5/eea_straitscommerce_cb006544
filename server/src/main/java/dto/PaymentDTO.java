package dto;

import com.deltahaze.straitscommerce.Models.Orders;
import com.deltahaze.straitscommerce.Models.User;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PaymentDTO {
    private Integer id;
    private String payment_type;
    private String card_type;
    private String card_name;
    private String card_number;
    private String card_expiry_date;
    private String cvv;

    private User user;

    private List<Orders> orders;

    public PaymentDTO() {
    }
}
