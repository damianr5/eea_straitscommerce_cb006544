package dto;

import com.deltahaze.straitscommerce.Models.Address;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ProductDTO {
    private Integer id;
    private String product_name;
    private int quantity;
    private double price;
    private String details;
    private String product_scaled_image;
    private String product_enlarged_image;

    private List<Address> address;

    public ProductDTO() {
    }
}
