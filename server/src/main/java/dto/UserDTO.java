package dto;

import com.deltahaze.straitscommerce.Models.Address;
import com.deltahaze.straitscommerce.Models.Cart;
import com.deltahaze.straitscommerce.Models.Orders;
import com.deltahaze.straitscommerce.Models.Payment;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserDTO {
    private Integer id;
    private String first_name;
    private String last_name;
    private String username;
    private String email;
    private String password;
    private String dob;
    private int user_type;

    private List<OrdersDTO> order;

    private Cart cart;

    private List<Payment> payments;

    private List<Address> addresses;

    public UserDTO() {
    }
}
