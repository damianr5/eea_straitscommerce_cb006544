package com.example.straits_commerce_mobile.Model;

public class Product {
    private Integer id;
    private String product_name;
    private int quantity;
    private double price;
    private String details;
    private String product_scaled_image;
    private String product_enlarged_image;

    public Product(){

    }

    public Product(Integer id, String product_name, int quantity, double price, String details, String product_scaled_image, String product_enlarged_image) {
        this.id = id;
        this.product_name = product_name;
        this.quantity = quantity;
        this.price = price;
        this.details = details;
        this.product_scaled_image = product_scaled_image;
        this.product_enlarged_image = product_enlarged_image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getProduct_scaled_image() {
        return product_scaled_image;
    }

    public void setProduct_scaled_image(String product_scaled_image) {
        this.product_scaled_image = product_scaled_image;
    }

    public String getProduct_enlarged_image() {
        return product_enlarged_image;
    }

    public void setProduct_enlarged_image(String product_enlarged_image) {
        this.product_enlarged_image = product_enlarged_image;
    }
}
