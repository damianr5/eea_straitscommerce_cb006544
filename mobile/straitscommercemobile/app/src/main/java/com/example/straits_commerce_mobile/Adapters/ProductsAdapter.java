package com.example.straits_commerce_mobile.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.straits_commerce_mobile.Activities.DetailedProductActivity;
import com.example.straits_commerce_mobile.Model.Product;
import com.example.straits_commerce_mobile.R;

import java.util.ArrayList;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.RequestViewHolder> {

    private ArrayList<Product> items = new ArrayList<>();

    Context mContext;

    public ProductsAdapter(Context mContext, ArrayList<Product> items) {
        this.mContext = mContext;
        this.items = items;
    }

    @Override
    public RequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.recycler_view, null);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);
        RequestViewHolder viewHolder = new RequestViewHolder(view);

        return new RequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RequestViewHolder holder, int position) {
        final Product product = items.get(position);


        String name = product.getProduct_name();

        String image = product.getProduct_scaled_image();


        holder.grid_name.setText(name);

        Glide.with(mContext)
                .asBitmap()
                .load(image)
                .into(holder.imageView);


        holder.item_grid_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productId = product.getId().toString();

                Intent intent = new Intent(mContext, DetailedProductActivity.class);

                intent.putExtra("this_product", productId);

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class RequestViewHolder extends RecyclerView.ViewHolder {

        public View vm;

        TextView grid_name;
        ImageView imageView;
        LinearLayout item_grid_layout;

        public RequestViewHolder(View itemView) {
            super(itemView);

            grid_name = itemView.findViewById(R.id.grid__product_name);
            imageView = itemView.findViewById(R.id.grid_product_image);
            item_grid_layout = itemView.findViewById(R.id.item_grid_layout);

        }

    }
}
