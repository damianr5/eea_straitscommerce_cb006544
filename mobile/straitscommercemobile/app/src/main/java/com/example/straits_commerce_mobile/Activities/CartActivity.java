package com.example.straits_commerce_mobile.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.straits_commerce_mobile.APIService;
import com.example.straits_commerce_mobile.Adapters.CartAdapter;
import com.example.straits_commerce_mobile.Model.Cart;
import com.example.straits_commerce_mobile.Model.Product;
import com.example.straits_commerce_mobile.Model.User;
import com.example.straits_commerce_mobile.R;
import com.example.straits_commerce_mobile.SharedPreferenceHandler;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CartActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private SharedPreferenceHandler session;
    private String URL;
    Button goToCheckoutButton;
    private static Retrofit retrofit = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        recyclerView = (RecyclerView) findViewById(R.id.cartRV);

        session = new SharedPreferenceHandler(getApplicationContext());

        int id = Integer.parseInt(session.getUserId());

        goToCheckoutButton = (Button) findViewById(R.id.checkoutBtn);

        URL = "http://10.0.2.2:8080/";

        getCart(id);

        goToCheckoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CartActivity.this, CheckoutActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public Cart getCart(int id){
        final Cart cart = new Cart();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create()).build();
        }

        APIService apiService = retrofit.create(APIService.class);
        Call<Cart> call = apiService.getCart(id);

        call.enqueue(new Callback<Cart>() {
            @Override
            public void onResponse(Call<Cart> call, Response<Cart> response) {
                try {
                    response.body();
                    User user = new User();
                    user.setId(response.body().getUser().getId());

                    ArrayList<Product> list = new ArrayList<>();
                    for(int i = 0; i<response.body().getProducts().size(); i++)
                    {
                        Integer id = response.body().getProducts().get(i).getId();
                        String name = response.body().getProducts().get(i).getProduct_name();
                        String description = response.body().getProducts().get(i).getDetails();
                        int quantity = response.body().getProducts().get(i).getQuantity();
                        double price = response.body().getProducts().get(i).getPrice();
                        String scaledImage = response.body().getProducts().get(i).getProduct_scaled_image();
                        String fullImage = response.body().getProducts().get(i).getProduct_enlarged_image();
                        list.add(new Product(
                                id,
                                name,
                                quantity,
                                price,
                                description,
                                scaledImage,
                                fullImage
                        ));
                        recyclerView.setLayoutManager(new GridLayoutManager(CartActivity.this, 1));
                        recyclerView.addItemDecoration(new SampleItemDecoration());
                        CartAdapter cartAdapter = new CartAdapter(getApplicationContext(),list);

                        recyclerView.setAdapter(cartAdapter);

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Cart> call, Throwable t) {
                t.printStackTrace();
            }
        });
        return cart;
    }

    class SampleItemDecoration extends RecyclerView.ItemDecoration {

        Paint paint = new Paint();

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
            for (int i = 0; i < parent.getChildCount(); i++) {
                View view = parent.getChildAt(i);
                paint.setColor(Color.parseColor("#E0E0E0"));
                if (parent.getChildLayoutPosition(view) == RecyclerView.NO_POSITION) {
                    continue;
                }

                // Compute bounds of cell in layout
                Rect bounds = new Rect(
                        layoutManager.getDecoratedLeft(view),
                        layoutManager.getDecoratedTop(view),
                        layoutManager.getDecoratedRight(view),
                        layoutManager.getDecoratedBottom(view)
                );

                // Add space between cell backgrounds
                bounds.inset(2, 2);

                c.drawRect(bounds, paint);
            }
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(10, 10, 10, 10); // Specify spacing between items in grid
        }
    }
}
