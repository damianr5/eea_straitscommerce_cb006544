package com.example.straits_commerce_mobile.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.straits_commerce_mobile.APIService;
import com.example.straits_commerce_mobile.Model.User;
import com.example.straits_commerce_mobile.R;
import com.example.straits_commerce_mobile.SharedPreferenceHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProfileActivity extends AppCompatActivity {

    TextView firstnameEditText, lastnameEditText, usernameEditText, emailEditText, passwordEditText, dobEditText;
    private String URL;
    private static Retrofit retrofit = null;
    private SharedPreferenceHandler session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        firstnameEditText = (EditText) findViewById(R.id.viewFirstName);
        lastnameEditText = (EditText) findViewById(R.id.viewLastName);
        usernameEditText = (EditText) findViewById(R.id.viewUsername);
        emailEditText = (EditText) findViewById(R.id.viewEmail);
        passwordEditText = (EditText) findViewById(R.id.viewPassword);
        dobEditText = (EditText) findViewById(R.id.viewDOB);

        session = new SharedPreferenceHandler(getApplicationContext());
        int userId = Integer.valueOf(session.getUserId());

        URL = "http://10.0.2.2:8080/";

        getUser(userId);
    }

    public User getUser(int id) {

        final User user = new User();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        APIService apiService = retrofit.create(APIService.class);
        Call<User> call = apiService.getUser(id);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                try {
                    response.body();
                    firstnameEditText.setText("First Name: "+response.body().getFirst_name());
                    lastnameEditText.setText("Last Name: "+response.body().getLast_name());
                    usernameEditText.setText("Username: "+response.body().getUsername());
                    emailEditText.setText("Email: "+response.body().getEmail());
                    passwordEditText.setText(response.body().getPassword());
                    dobEditText.setText("DOB: "+response.body().getDob());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

        return user;

    }
}
