package com.example.straits_commerce_mobile;

import com.example.straits_commerce_mobile.Model.Cart;
import com.example.straits_commerce_mobile.Model.Orders;
import com.example.straits_commerce_mobile.Model.Product;
import com.example.straits_commerce_mobile.Model.User;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface APIService {
    //User Related Access Points
    @GET("/users")
    Call<List<User>> getUsers();

    @GET("users/{id}")
    Call <User> getUser(@Path("id") int id);

    @POST("/users")
    Call <User> registerUser(@Query("firstName") String first_name, @Query("lastName") String last_name, @Query("username") String username, @Query("email") String email,@Query("password") String password, @Query("dob") String dob);

    @PUT("/users")
    Call <User> updateUser(@QueryMap Map<String, String> body);


    //Login related Access points
    @POST("/login")
    Call<String> login(@Query("username") String username, @Query("password") String password);

    //Products related Access points
    @GET("products")
    Call<List<Product>> getProducts();

    @GET("/products/{id}")
    Call<Product> getProduct(@Path("id") int id);

    //Cart related calls
    @POST("/cart")
    Call<Cart> generateCart(@Body Cart cart);

    @GET("cart/{id}")
    Call<Cart> getCart(@Path("id") int id);

    //Order related calls
    @POST("/orders")
    Call<Orders> generateOrder(@Body Orders orders);
}
