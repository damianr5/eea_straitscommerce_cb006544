package com.example.straits_commerce_mobile.Model;

import java.util.List;

public class Cart {
    private Integer id;
    private int quantity;
    private double cart_total;

    public Cart(){

    }

    public Cart(Integer id, int quantity, double cart_total) {
        this.id = id;
        this.quantity = quantity;
        this.cart_total = cart_total;
    }

    private User user;

    private Orders order;

    private List<Product> products;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getCart_total() {
        return cart_total;
    }

    public void setCart_total(double cart_total) {
        this.cart_total = cart_total;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Orders getOrder() {
        return order;
    }

    public void setOrder(Orders order) {
        this.order = order;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
