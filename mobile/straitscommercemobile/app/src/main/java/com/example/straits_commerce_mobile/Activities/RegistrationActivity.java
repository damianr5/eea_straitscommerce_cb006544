package com.example.straits_commerce_mobile.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.straits_commerce_mobile.APIService;
import com.example.straits_commerce_mobile.Model.User;
import com.example.straits_commerce_mobile.R;
import com.example.straits_commerce_mobile.SharedPreferenceHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegistrationActivity extends AppCompatActivity {

    EditText firstName, lastName, username, password, email, dob;
    Button register;
    private SharedPreferenceHandler session;
    private String URL;
    private static Retrofit retrofit = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        firstName = (EditText) findViewById(R.id.textRegFirstName);
        lastName = (EditText) findViewById(R.id.textRegLastName);
        username = (EditText) findViewById(R.id.textRegUsername);
        password = (EditText) findViewById(R.id.textRegPassword);
        email = (EditText) findViewById(R.id.textEmail);
        dob = (EditText) findViewById(R.id.textRegDOB);

        register = (Button) findViewById(R.id.bSignup);

        session = new SharedPreferenceHandler(getApplicationContext());
        URL = "http://10.0.2.2:8080/";

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Register( firstName.getText().toString(),lastName.getText().toString(),username.getText().toString(),email.getText().toString(), password.getText().toString(),dob.getText().toString());
            }
        });
    }

    public void Register (String FirstName, String LastName, String username, String password, String email, String DOB){
        if (retrofit == null){
            retrofit = new Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create()).build();
        }
        APIService apiService = retrofit.create(APIService.class);

        Call<User> call = apiService.registerUser(FirstName,LastName,username,password,email,DOB);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                if(response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "User Registered Successfully!", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "User Already exists", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Server Error!", Toast.LENGTH_LONG).show();
            }
        });
    }
}
