package com.example.straits_commerce_mobile.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;

import com.example.straits_commerce_mobile.APIService;
import com.example.straits_commerce_mobile.Adapters.ProductsAdapter;
import com.example.straits_commerce_mobile.Model.Product;
import com.example.straits_commerce_mobile.R;
import com.example.straits_commerce_mobile.SharedPreferenceHandler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private SharedPreferenceHandler session;
    private String URL;
    private static Retrofit retrofit = null;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        recyclerView = findViewById(R.id.productRV);
        session = new SharedPreferenceHandler(getApplicationContext());
        URL = "http://10.0.2.2:8080/";
        fab = (FloatingActionButton) findViewById(R.id.floating_action_button) ;
        ArrayList<Product> products = getAllProducts();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, CartActivity.class);
                startActivity(intent);
            }
        });
    }

    public ArrayList<Product> getAllProducts() {
        final ArrayList<Product> products = new ArrayList<>();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        APIService apiService = retrofit.create(APIService.class);
        Call<List<Product>> call = apiService.getProducts();

        call.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                try {


                    products.addAll(response.body());
                    ArrayList<Product> list = new ArrayList<>();
                    for (int i = 0; i < products.size(); i++) {
                        Integer id = products.get(i).getId();
                        String name = products.get(i).getProduct_name();


                        String details = products.get(i).getDetails();
                        int quantity = products.get(i).getQuantity();
                        double price = products.get(i).getPrice();
                        String scaledImage = products.get(i).getProduct_scaled_image();
                        String fullImage = products.get(i).getProduct_scaled_image();

                        list.add(new Product(
                                id,
                                name,
                                quantity,
                                price,
                                details,
                                scaledImage,
                                fullImage
                        ));

                        recyclerView.setLayoutManager(new GridLayoutManager(HomeActivity.this, 2));
                        recyclerView.addItemDecoration(new SampleItemDecoration());

                        ProductsAdapter productListAdapter = new ProductsAdapter(getApplicationContext(), list);

                        recyclerView.setAdapter(productListAdapter);

                    }

                } catch (Exception e) {
                    Log.d("onResponse", "Sorry but products was not found :(");
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.e("TAG", t.toString());
            }
        });


        return products;
    }

    class SampleItemDecoration extends RecyclerView.ItemDecoration {
        Paint paint = new Paint();

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
            for (int i = 0; i < parent.getChildCount(); i++) {
                View view = parent.getChildAt(i);
                paint.setColor(Color.parseColor("#FFFFFF"));
                if (parent.getChildLayoutPosition(view) == RecyclerView.NO_POSITION) {
                    continue;
                }

                // Compute bounds of cell in layout
                Rect bounds = new Rect(
                        layoutManager.getDecoratedLeft(view),
                        layoutManager.getDecoratedTop(view),
                        layoutManager.getDecoratedRight(view),
                        layoutManager.getDecoratedBottom(view)
                );

                // Add space between cell backgrounds
                bounds.inset(2, 2);

                c.drawRect(bounds, paint);
            }
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(10, 10, 10, 10); // Specify spacing between items in grid
        }
    }
}
