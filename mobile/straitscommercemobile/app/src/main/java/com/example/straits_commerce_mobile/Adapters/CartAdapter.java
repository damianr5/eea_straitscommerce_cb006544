package com.example.straits_commerce_mobile.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.straits_commerce_mobile.Model.Product;
import com.example.straits_commerce_mobile.R;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.RequestViewHolder>{
    private ArrayList<Product> items = new ArrayList<>();
    Context mContext;

    public CartAdapter(Context mContext,ArrayList<Product> items)
    {
        this.mContext=mContext;
        this.items=items;
    }

    @Override
    public RequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.cart_recycler_view, null);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(layoutParams);
        RequestViewHolder viewHolder = new RequestViewHolder(view);

        return new RequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RequestViewHolder holder, int position)
    {
        final Product product=items.get(position);


        String name=product.getProduct_name();

        String price= Double.toString(product.getPrice());

        String imageLink=product.getProduct_scaled_image();

        holder.cart_name.setText(name);
        holder.cart_price.setText("$"+price);

        Glide.with(mContext)
                .asBitmap()
                .load(imageLink)
                .into(holder.cart_image);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class RequestViewHolder extends RecyclerView.ViewHolder
    {

        public View vm;

        TextView cart_name;
        TextView cart_price;
        ImageView cart_image;
        LinearLayout cart_item_layout;

        public RequestViewHolder(View itemView)
        {
            super(itemView);

            cart_name =itemView.findViewById(R.id.cart_name);
            cart_price =itemView.findViewById(R.id.cart_price);
            cart_image =itemView.findViewById(R.id.cart_image);
            cart_item_layout =itemView.findViewById(R.id.item_grid_layout);
        }

    }
}
