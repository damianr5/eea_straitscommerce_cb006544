package com.example.straits_commerce_mobile.Model;

public class Payment {
    private Integer id;
    private String payment_type;
    private String card_type;
    private String card_name;
    private String card_number;
    private String card_expiry_date;
    private String cvv;

    public Payment(){

    }

    public Payment(Integer id, String payment_type, String card_type, String card_name, String card_number, String card_expiry_date, String cvv) {
        this.id = id;
        this.payment_type = payment_type;
        this.card_type = card_type;
        this.card_name = card_name;
        this.card_number = card_number;
        this.card_expiry_date = card_expiry_date;
        this.cvv = cvv;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCard_expiry_date() {
        return card_expiry_date;
    }

    public void setCard_expiry_date(String card_expiry_date) {
        this.card_expiry_date = card_expiry_date;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }
}
