package com.example.straits_commerce_mobile.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.straits_commerce_mobile.APIService;
import com.example.straits_commerce_mobile.Model.Cart;
import com.example.straits_commerce_mobile.Model.Product;
import com.example.straits_commerce_mobile.Model.User;
import com.example.straits_commerce_mobile.R;
import com.example.straits_commerce_mobile.SharedPreferenceHandler;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailedProductActivity extends AppCompatActivity {

    TextView nameTextView, descriptionTextView, priceTextView, quantityTextView;
    ImageView image;
    Button AddToCart;
    Cart cart;
    private SharedPreferenceHandler session;
    private String URL;
    private static Retrofit retrofit = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_product);

        int id = Integer.parseInt(getIntent().getStringExtra("this_product"));

        session = new SharedPreferenceHandler(getApplicationContext());
        Integer cartUserId = Integer.valueOf(session.getUserId());
        Integer cartProductId = id;

        cart = new Cart();
        User cartUser = new User();
        cartUser.setId(cartUserId);

        Product cartProduct = new Product();
        cartProduct.setId(cartProductId);

        List<Product> cartProductList = new ArrayList<>();
        cartProductList.add(cartProduct);

        cart.setUser(cartUser);
        cart.setProducts(cartProductList);

        URL = "http://10.0.2.2:8080/";

        nameTextView = (TextView) findViewById(R.id.name_text);
        descriptionTextView = (TextView) findViewById(R.id.description_text);
        priceTextView = (TextView) findViewById(R.id.price_text);
        quantityTextView = (TextView) findViewById(R.id.quantity_text);

        image = (ImageView) findViewById(R.id.product_image);

        AddToCart = (Button) findViewById(R.id.addToCartBtn);

        getProduct(id);

        AddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addProductToCart(cart);
            }
        });
    }

    public void getProduct(int id){
        final Product product = new Product();

        if(retrofit==null){
            retrofit = new Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create()).build();
        }

        APIService apiService = retrofit.create(APIService.class);
        Call<Product> call = apiService.getProduct(id);
        call.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                try {
                    response.body();
                    String name = response.body().getProduct_name();
                    String description = response.body().getDetails();
                    String price = Double.toString(response.body().getPrice());
                    String quantity = Integer.toString(response.body().getQuantity());
                    String img = response.body().getProduct_scaled_image();

                    nameTextView.setText(name);
                    descriptionTextView.setText(description);
                    priceTextView.setText("$"+price);
                    quantityTextView.setText(quantity+" left");

                    Glide.with(DetailedProductActivity.this).asBitmap().load(img).into(image);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Log.e("Error loading Product", t.toString());
            }
        });
    }

    public void addProductToCart(Cart cart){
        if (retrofit==null){
            retrofit = new Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create()).build();
        }

        APIService apiService = retrofit.create(APIService.class);
        Call<Cart> call = apiService.generateCart(cart);
        call.enqueue(new Callback<Cart>() {
            @Override
            public void onResponse(Call<Cart> call, Response<Cart> response) {
                if(response.isSuccessful()){
                    Toast.makeText(getApplicationContext(), "Product Added To Cart Successfully", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(getApplicationContext(),HomeActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Error Adding Product To Cart", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Cart> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error connecting to server", Toast.LENGTH_LONG).show();
            }
        });
    }
}
