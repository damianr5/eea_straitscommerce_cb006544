package com.example.straits_commerce_mobile.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.straits_commerce_mobile.APIService;
import com.example.straits_commerce_mobile.Model.Address;
import com.example.straits_commerce_mobile.Model.Cart;
import com.example.straits_commerce_mobile.Model.Orders;
import com.example.straits_commerce_mobile.Model.Payment;
import com.example.straits_commerce_mobile.Model.User;
import com.example.straits_commerce_mobile.R;
import com.example.straits_commerce_mobile.SharedPreferenceHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CheckoutActivity extends AppCompatActivity {

    EditText street1EditText, street2EditText, cityEditText, countryEditText, zipcodeEditText;
    EditText paymentmethodEditText, cardtypeEditText, cardNameEditText, cardNumberEditText, cardExpiryEditText, cvvEditText;
    Button confirmOrderBtn;
    Orders orders;
    Payment payment;
    User user;
    Cart cart;
    Address address;
    private SharedPreferenceHandler session;
    private String URL;
    private static Retrofit retrofit = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        street1EditText       = (EditText) findViewById(R.id.textStreet1);
        street2EditText       = (EditText) findViewById(R.id.textStreet2);
        cityEditText          = (EditText) findViewById(R.id.textCity);
        zipcodeEditText       = (EditText) findViewById(R.id.textZipCode);
        countryEditText       = (EditText) findViewById(R.id.textCountry);

        paymentmethodEditText = (EditText) findViewById(R.id.textPaymentMethod);
        cardtypeEditText       = (EditText) findViewById(R.id.textCardType);
        cardNameEditText    = (EditText) findViewById(R.id.textCardName);
        cardNumberEditText    = (EditText) findViewById(R.id.textCardNumber);
        cardExpiryEditText   = (EditText) findViewById(R.id.textCardExpDate);
        cvvEditText           = (EditText) findViewById(R.id.textCVV);

        confirmOrderBtn   = (Button) findViewById(R.id.bConfirmCheckout);

        URL = "http://10.0.2.2:8080/";

        confirmOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session = new SharedPreferenceHandler(getApplicationContext());

                int id = Integer.parseInt(session.getUserId());
                cart = new Cart();
                user = new User();

                user.setId(id);

                cart.setUser(user);

                address = new Address();

                address.setStreet1(street1EditText.getText().toString());
                address.setStreet2(street2EditText.getText().toString());
                address.setCity(cityEditText.getText().toString());
                address.setZip_code(zipcodeEditText.getText().toString());
                address.setCountry(countryEditText.getText().toString());

                payment = new Payment();

                payment.setPayment_type(paymentmethodEditText.getText().toString());
                payment.setCard_type(cardtypeEditText.getText().toString());
                payment.setCard_name(cardNameEditText.getText().toString());
                payment.setCard_number(cardNumberEditText.getText().toString());
                payment.setCard_expiry_date(cardExpiryEditText.getText().toString());
                payment.setCvv(cvvEditText.getText().toString());

                orders = new Orders();

                orders.setCart(cart);
                orders.setAddresses(address);
                orders.setPayment(payment);
                createOrder(orders);
            }
        });
    }

    public void createOrder (Orders orders){
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        APIService apiService = retrofit.create(APIService.class);

        Call<Orders> call = apiService.generateOrder(orders);
        call.enqueue(new Callback<Orders>() {
            @Override
            public void onResponse(Call<Orders> call, Response<Orders> response) {
                if(response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Order Successfully Placed!", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(getApplicationContext(),CartActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Something went wrong, please try again", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Orders> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error connecting to server", Toast.LENGTH_LONG).show();
            }
        });
    }
}
