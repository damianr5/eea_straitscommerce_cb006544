package com.example.straits_commerce_mobile.Model;

public class Orders {
    private Integer id;
    private int no_of_items;
    private double total_price;
    private String order_date;

    public Orders(){

    }

    public Orders(Integer id, int no_of_items, double total_price, String order_date) {
        this.id = id;
        this.no_of_items = no_of_items;
        this.total_price = total_price;
        this.order_date = order_date;
    }

    private Cart cart;

    private User user;

    private Payment payment;

    private Address addresses;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getNo_of_items() {
        return no_of_items;
    }

    public void setNo_of_items(int no_of_items) {
        this.no_of_items = no_of_items;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Address getAddresses() {
        return addresses;
    }

    public void setAddresses(Address addresses) {
        this.addresses = addresses;
    }
}
