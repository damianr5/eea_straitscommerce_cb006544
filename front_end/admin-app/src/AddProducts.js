import React, {Component} from "react";

class AddProducts extends Component{

    constructor(props){
        super(props);
        this.state = {product_name: '', details:'', quantity:'', price:'', product_scaled_image:'', product_enlarged_image:''}
        this.changeHandler= this.changeHandler.bind(this);
        this.handleProducts = this.handleProducts.bind(this);
    }
    changeHandler = event =>
        this.setState({[event.target.name]: event.target.value});

    handleProducts(event){
        event.preventDefault();
        const data = new FormData(event.target);
        const product = {
            product_name:data.get("productName"),
            details: data.get("productDescription"),
            quantity: data.get("productQuantity"),
            price: data.get("productPrice"),
            product_scaled_image: data.get("productScaled"),
            product_enlarged_image: data.get("productFull")

        };
        fetch("http://localhost:8080/products",{
            method: "POST",
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(product)
        }).then(response =>{
            alert("Product has been Successfully Added!")
            window.location="Dashboard"
        })
    }
    render() {
        return (
            <div>
                <title>Straits Admin Panel</title>
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
                {/*Google Fonts*/}
                <link href="//fonts.googleapis.com/css?family=Carrois+Gothic" rel="stylesheet" type="text/css" />
                <link href="//fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet" type="text/css" />

                <div className="page-container">
                    <div className="left-content">
                        <div className="mother-grid-inner">
                            {/*header start here*/}
                            <div className="header-main">
                                <div className="header-left">
                                    <h1>Add Products</h1>
                                    <div className="clearfix"> </div>
                                </div>
                                <div className="header-right">
                                    <div className="profile_details_left">{/*notifications of menu start */}

                                        <div className="clearfix"> </div>
                                    </div>
                                    {/*notification menu end */}
                                    <div className="profile_details">
                                        <ul>
                                            <li className="dropdown profile_details_drop">
                                                <a className="dropdown-toggle">
                                                    <div className="profile_img">
                                                        <div className="user-name">
                                                            <span>Administrator</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="clearfix"> </div>
                                </div>
                                <div className="clearfix"> </div>
                            </div>
                            {/*heder end here*/}
                            {/*inner block start here*/}
                            <div className="inner-block">

                                {/*main page chart layer2*/}
                                <div className="chart-layer-2">
                                    <div className="col-md-12 chart-layer2-right">
                                        <form className="row login_form" onSubmit={this.handleProducts}>
                                            <div className="col-md-12 form-group">
                                                <input type="text" className="form-control" id="name"
                                                       name="productName" placeholder="Product Name"
                                                       onFocus="this.placeholder = ''"
                                                       onBlur="this.placeholder = 'Product Name'"/>
                                            </div>
                                            <div className="col-md-12 form-group">
                                                <input type="text" className="form-control" id="description" name="productDescription"
                                                       placeholder="Description" onFocus="this.placeholder = ''"
                                                       onBlur="this.placeholder = 'Description'"/>
                                            </div>
                                            <div className="col-md-12 form-group">
                                                <input type="text" className="form-control" id="quantity"
                                                       name="productQuantity" placeholder="Quantity"
                                                       onFocus="this.placeholder = ''"
                                                       onBlur="this.placeholder = 'Quantity'"/>
                                            </div>
                                            <div className="col-md-12 form-group">
                                                <input type="text" className="form-control" id="price" name="productPrice"
                                                       placeholder="Price" onFocus="this.placeholder = ''"
                                                       onBlur="this.placeholder = 'Price'"/>
                                            </div>
                                            <div className="col-md-12 form-group">
                                                <input type="text" className="form-control" id="scaledImage" name="productScaled"
                                                       placeholder="Image" onFocus="this.placeholder = ''"
                                                       onBlur="this.placeholder = 'Price'"/>
                                            </div>
                                            <div className="col-md-12 form-group">
                                                <input type="text" className="form-control" id="fullImage" name="productFull"
                                                       placeholder="Image" onFocus="this.placeholder = ''"
                                                       onBlur="this.placeholder = 'Price'"/>
                                            </div>
                                            <div className="col-md-12 form-group">
                                                <button type="submit" className="button button-register w-100">Add product
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div className="col-md-6 chart-layer2-left">
                                        <canvas id="radar" height={300} width={300} style={{width: '300px', height: '300px'}} />
                                    </div>
                                    <div className="clearfix"> </div>
                                </div>

                            </div>
                            {/*inner block end here*/}
                        </div>
                    </div>
                    {/*slider menu*/}
                    <div className="sidebar-menu">
                        <img className="dash-logo" src={require("./img/logo.png")} alt=""/>
                        <div className="menu">
                            <ul id="menu">
                                <li id="menu-home"><a href="index.html"><i className="fa fa-tachometer" /><span>Dashboard</span></a></li>



                                <li><a href="charts.html"><i className="fa fa-bar-chart" /><span>Charts</span></a></li>
                                <li><a href="#"><i className="fa fa-envelope" /><span>Mailbox</span><span className="fa fa-angle-right" style={{float: 'right'}} /></a>
                                    <ul id="menu-academico-sub">
                                        <li id="menu-academico-avaliacoes"><a href="inbox.html">Inbox</a></li>
                                        <li id="menu-academico-boletim"><a href="inbox-details.html">Compose email</a></li>
                                    </ul>
                                </li>
                                <li><a href="#"><i className="fa fa-shopping-cart" /><span>E-Commerce</span><span className="fa fa-angle-right" style={{float: 'right'}} /></a>
                                    <ul id="menu-academico-sub">
                                        <li id="menu-academico-avaliacoes"><a href="/addproduct">Add Products</a></li>
                                        <li id="menu-academico-boletim"><a href="/users">View Users</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="clearfix"> </div>
                </div>
            </div>
        );
    }
}

export default AddProducts;