import React, {Component} from "react";

class Dashboard extends Component{
        render() {
            return (
                <div>
                    <title>Straits Admin Panel</title>
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
                    {/*Google Fonts*/}
                    <link href="//fonts.googleapis.com/css?family=Carrois+Gothic" rel="stylesheet" type="text/css" />
                    <link href="//fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet" type="text/css" />

                    <div className="page-container">
                        <div className="left-content">
                            <div className="mother-grid-inner">
                                {/*header start here*/}
                                <div className="header-main">
                                    <div className="header-left">
                                        <div className="clearfix"> </div>
                                    </div>
                                    <div className="header-right">
                                        <div className="profile_details_left">{/*notifications of menu start */}

                                            <div className="clearfix"> </div>
                                        </div>
                                        {/*notification menu end */}
                                        <div className="profile_details">
                                            <ul>
                                                <li className="dropdown profile_details_drop">
                                                    <a className="dropdown-toggle">
                                                        <div className="profile_img">
                                                            <div className="user-name">
                                                                <p>Paula</p>
                                                                <span>Administrator</span>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="clearfix"> </div>
                                    </div>
                                    <div className="clearfix"> </div>
                                </div>
                                {/*heder end here*/}
                                {/*inner block start here*/}
                                <div className="inner-block">
                                    {/*market updates updates*/}
                                    <div className="market-updates">
                                        <div className="col-md-4 market-update-gd">
                                            <div className="market-update-block clr-block-1">
                                                <div className="col-md-8 market-update-left">
                                                    <h3>83</h3>
                                                    <h4>Registered User</h4>
                                                    <p>Other hand, we denounce</p>
                                                </div>
                                                <div className="col-md-4 market-update-right">
                                                    <i className="fa fa-file-text-o"> </i>
                                                </div>
                                                <div className="clearfix"> </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 market-update-gd">
                                            <div className="market-update-block clr-block-2">
                                                <div className="col-md-8 market-update-left">
                                                    <h3>135</h3>
                                                    <h4>Daily Visitors</h4>
                                                    <p>Other hand, we denounce</p>
                                                </div>
                                                <div className="col-md-4 market-update-right">
                                                    <i className="fa fa-eye"> </i>
                                                </div>
                                                <div className="clearfix"> </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 market-update-gd">
                                            <div className="market-update-block clr-block-3">
                                                <div className="col-md-8 market-update-left">
                                                    <h3>23</h3>
                                                    <h4>New Messages</h4>
                                                    <p>Other hand, we denounce</p>
                                                </div>
                                                <div className="col-md-4 market-update-right">
                                                    <i className="fa fa-envelope-o"> </i>
                                                </div>
                                                <div className="clearfix"> </div>
                                            </div>
                                        </div>
                                        <div className="clearfix"> </div>
                                    </div>

                                    {/*main page chart layer2*/}
                                    <div className="chart-layer-2">
                                        <div className="col-md-6 chart-layer2-right">
                                            <div className="prograc-blocks">
                                                {/*Progress bars*/}
                                                <div className="home-progres-main">
                                                    <h3>Total Sales</h3>
                                                </div>
                                                <div className="bar_group">
                                                    <div className="bar_group__bar thin" label="Rating" show_values="true" tooltip="true" value={343} />
                                                    <div className="bar_group__bar thin" label="Quality" show_values="true" tooltip="true" value={235} />
                                                    <div className="bar_group__bar thin" label="Amount" show_values="true" tooltip="true" value={550} />
                                                    <div className="bar_group__bar thin" label="Farming" show_values="true" tooltip="true" value={456} />
                                                </div>
                                                {/*//Progress bars*/}
                                            </div>
                                        </div>
                                        <div className="col-md-6 chart-layer2-left">
                                            <div className="content-main revenue">
                                                <h3>Total Revenue</h3>
                                                <canvas id="radar" height={300} width={300} style={{width: '300px', height: '300px'}} />
                                            </div>
                                        </div>
                                        <div className="clearfix"> </div>
                                    </div>

                                </div>
                                {/*inner block end here*/}
                            </div>
                        </div>
                        {/*slider menu*/}
                        <div className="sidebar-menu">
                            <img className="dash-logo" src={require("./img/logo.png")} alt=""/>
                            <div className="menu">
                                <ul id="menu">
                                    <li id="menu-home"><a href="index.html"><i className="fa fa-tachometer" /><span>Dashboard</span></a></li>



                                    <li><a href="charts.html"><i className="fa fa-bar-chart" /><span>Charts</span></a></li>
                                    <li><a href="#"><i className="fa fa-envelope" /><span>Mailbox</span><span className="fa fa-angle-right" style={{float: 'right'}} /></a>
                                        <ul id="menu-academico-sub">
                                            <li id="menu-academico-avaliacoes"><a href="inbox.html">Inbox</a></li>
                                            <li id="menu-academico-boletim"><a href="inbox-details.html">Compose email</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#"><i className="fa fa-shopping-cart" /><span>E-Commerce</span><span className="fa fa-angle-right" style={{float: 'right'}} /></a>
                                        <ul id="menu-academico-sub">
                                            <li id="menu-academico-avaliacoes"><a href="/addproduct">Add Products</a></li>
                                            <li id="menu-academico-boletim"><a href="/users">View Users</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="clearfix"> </div>
                    </div>
                </div>
            );
        }
}

export default Dashboard;