import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Login from './Login';
import Dashboard from "./Dashboard";
import AddProducts from "./AddProducts"

class App extends Component {
  render() {
    return (
        <Router>
          <Switch>
            <Route path='/' exact={true} component={Login}/>
              <Route path='/dashboard' exact={true} component={Dashboard}/>
              <Route path='/addproduct' exact={true} component={AddProducts}/>
          </Switch>
        </Router>
    )
  }
}

export default App;
