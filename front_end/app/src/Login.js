import React, { Component } from 'react';

class Login extends Component{

    constructor(props){
        super(props);
        this.state = {username: '', password: ''};
        this.changeHandler = this.changeHandler.bind(this);
        this.userLoginHandler = this.userLoginHandler.bind(this);

    }

    changeHandler = event=>
        this.setState({[event.target.name]: event.target.value});

    userLoginHandler(event){
        event.preventDefault();
        const data = new FormData(event.target);
        fetch("http://localhost:8080/login",{
            method: "POST",
            body: data
        })
            .then(response =>{
                if(response.status === 200)
                {
                    response.json().then(userId =>{
                        localStorage.setItem("userId", userId);
                        window.location.href = "/";
                    });
                }else{
                    window.location.href="/login";
                }
            });
    }

    render(){
        return (
            <div>
                {/* Required meta tags */}
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                <link rel="icon" href="img/favicon.png" type="image/png" />
                <title>Straits commerce</title>

                {/*================Header Menu Area =================*/}
                <header className="header_area">
                    <div className="top_menu">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-7">
                                    <div className="float-left">
                                        <p>Phone: +94 777 769 670</p>
                                        <p>email: sgt.info@straitsglobal.com</p>
                                    </div>
                                </div>
                                <div className="col-lg-5">
                                    <div className="float-right">
                                        <ul className="right_side">
                                            <li>
                                                <a href="contact.html">
                                                    Contact Us
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="main_menu_register">
                        <div className="container">
                            <nav className="navbar navbar-expand-lg navbar-light w-100">
                                {/* Brand and toggle get grouped for better mobile display */}
                                <a className="navbar-brand logo_h" href="/">
                                    <img className="logo" src={require("./img/logo.png")} alt="" />
                                </a>
                            </nav>
                        </div>
                    </div>

                    {/*================Login Area =================*/}
                    <div className="main-login">
                        <section className="signup">
                            <div className="container-signup">
                                <div className="signup-content">
                                    <form method="POST" id="signup-form" className="signup-form" onSubmit={this.userLoginHandler}>
                                        <h2 className="form-title">Login</h2>

                                        <div className="form-group">
                                            <input type="text" className="form-input" name="username" id="username" placeholder="Username" value={this.state.username} onChange={this.changeHandler}/>
                                        </div>

                                        <div className="form-group">
                                            <input type="password" className="form-input" name="password" id="password" placeholder="Password" value={this.state.password} onChange={this.changeHandler} />
                                            <span toggle="#password" className="zmdi zmdi-eye field-icon toggle-password" />
                                        </div>

                                        <button type="submit" value="submit" className="form-submit">Log
                                            In
                                        </button>
                                    </form>
                                    <p className="loginhere">
                                        Don't have an account? Don't worry just <a href="/register" className="loginhere-link">Register here</a>
                                    </p>
                                </div>
                            </div>
                        </section>
                    </div>
                </header>

            </div>
        );
    }
}

export default Login;