import React, {Component} from "react";

class Cart extends Component {

    constructor(props) {
        super(props);
        this.state = {cart: "", cartId: ""};
    }

    componentDidMount() {
        fetch(`http://localhost:8080/cart/${localStorage.getItem('userId')}`)
            .then(response => {
                return response.json();
            }).then(data => {
            this.setState(({
                cart: data
            }))
        })
    }

    render() {

        return (
            <div>
                {/* Required meta tags */}
                <meta charSet="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
                <link rel="icon" href="img/favicon.png" type="image/png"/>
                <title>Straits commerce</title>
                {/*================Header Menu Area =================*/}
                <header className="header_area">
                    <div className="top_menu">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-7">
                                    <div className="float-left">
                                        <p>Phone: +94 777 769 670</p>
                                        <p>email: sgt.info@straitsglobal.com</p>
                                    </div>
                                </div>
                                <div className="col-lg-5">
                                    <div className="float-right">
                                        <ul className="right_side">

                                            <li>
                                                <a href="contact.html">
                                                    Contact Us
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="main_menu">
                        <div className="container">
                            <nav className="navbar navbar-expand-lg navbar-light w-100">
                                {/* Brand and toggle get grouped for better mobile display */}
                                <a className="navbar-brand logo_h" href="/">
                                    <img className="logo" src={require("./img/logo.png")} alt=""/>
                                </a>
                                <button className="navbar-toggler" type="button" data-toggle="collapse"
                                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                        aria-expanded="false" aria-label="Toggle navigation">
                                    <span className="icon-bar"/>
                                    <span className="icon-bar"/>
                                    <span className="icon-bar"/>
                                </button>
                                {/* Collect the nav links, forms, and other content for toggling */}
                                <div className="collapse navbar-collapse offset w-100" id="navbarSupportedContent">
                                    <div className="row w-100 mr-0">
                                        <div className="col-lg-7 pr-0">
                                            <ul className="nav navbar-nav center_nav pull-right">
                                                <li className="nav-item">
                                                    <a className="nav-link" href="/">Home</a>
                                                </li>
                                                <li className="nav-item active submenu dropdown">
                                                    <a href="#" className="nav-link dropdown-toggle"
                                                       data-toggle="dropdown" role="button" aria-haspopup="true"
                                                       aria-expanded="false">Shop</a>
                                                    <ul className="dropdown-menu">
                                                        <li className="nav-item">
                                                            <a className="nav-link" href="category.html">Shop
                                                                Category</a>
                                                        </li>
                                                        <li className="nav-item">
                                                            <a className="nav-link" href="single-product.html">Product
                                                                Details</a>
                                                        </li>
                                                        <li className="nav-item">
                                                            <a className="nav-link" href="/checkout">Product
                                                                Checkout</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li className="nav-item submenu dropdown">
                                                    <a href="#" className="nav-link dropdown-toggle"
                                                       data-toggle="dropdown" role="button" aria-haspopup="true"
                                                       aria-expanded="false">Blog</a>
                                                    <ul className="dropdown-menu">
                                                        <li className="nav-item">
                                                            <a className="nav-link" href="blog.html">Blog</a>
                                                        </li>
                                                        <li className="nav-item">
                                                            <a className="nav-link" href="single-blog.html">Blog
                                                                Details</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li className="nav-item submenu dropdown">
                                                    <a href="#" className="nav-link dropdown-toggle"
                                                       data-toggle="dropdown" role="button" aria-haspopup="true"
                                                       aria-expanded="false">Pages</a>
                                                    <ul className="dropdown-menu">
                                                        <li className="nav-item">
                                                            <a className="nav-link" href="tracking.html">Tracking</a>
                                                        </li>
                                                        <li className="nav-item">
                                                            <a className="nav-link" href="elements.html">Elements</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li className="nav-item">
                                                    <a className="nav-link" href="contact.html">Contact</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="col-lg-5 pr-0">
                                            <ul className="nav navbar-nav navbar-right right_nav pull-right">
                                                <li className="nav-item">
                                                    <a href="#" className="icons">
                                                        <i className="ti-search" aria-hidden="true"/>
                                                    </a>
                                                </li>
                                                <li className="nav-item">
                                                    <a href="/profile" className="icons">
                                                        <i className="ti-user" aria-hidden="true"/>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </header>
                {/*================Header Menu Area =================*/}
                {/*================Home Banner Area =================*/}
                <section className="banner_area">
                    <div className="banner_inner d-flex align-items-center">
                        <div className="container">
                            <div className="banner_content d-md-flex justify-content-between align-items-center">
                                <div className="mb-3 mb-md-0">
                                    <h2>Cart</h2>
                                </div>
                                <div className="page_link">
                                    <a href="/">Home</a>
                                    <a href="/cart">Cart</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                {/*================End Home Banner Area =================*/}
                {/*================Cart Area =================*/}
                {this.state.cart !== "" && this.state.cart.products.map((prod, index) => {
                    return (

                        <section className="cart_area" key={index}>
                            <div className="container">
                                <div className="cart_inner">
                                    <div className="table-responsive">
                                        <table className="table">
                                            <thead>
                                            <tr>
                                                <th scope="col">Product</th>
                                                <th scope="col">Quantity</th>
                                                <th scope="col">Price</th>
                                                <th scope="col">Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <tr>
                                                <td>
                                                    <div className="media">
                                                        <img className="w-15" src={prod.product_scaled_image} alt=""/>

                                                        <div className="media-body">
                                                            <p>{prod.product_name}</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h5></h5>
                                                </td>
                                                <td>
                                                    <h5>${prod.price}</h5>
                                                </td>

                                                <td>
                                                    <h5>${prod.price}</h5>
                                                </td>
                                            </tr>

                                            <tr className="bottom_button">

                                                <td/>
                                                <td/>
                                            </tr>
                                            <tr>
                                                <td/>
                                                <td/>
                                                <td>
                                                    <h5>Subtotal</h5>
                                                </td>
                                                <td>
                                                    <h5>$2160.00</h5>
                                                </td>
                                            </tr>

                                            <tr className="out_button_area">
                                                <td/>
                                                <td/>
                                                <td/>
                                                <td>
                                                    <div className="checkout_btn_inner">
                                                        <a className="gray_btn " href="/">Continue Shopping <i
                                                            className="ti-shift-right-alt"></i></a>
                                                        <a className="main_btn" href="/checkout">Proceed to checkout</a>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    );
                })
                }
                {/*================End Cart Area =================*/}
                {/*================ start footer Area  =================*/}
                <footer className="footer-area section_gap">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-2 col-md-6 single-footer-widget">
                                <h4>Top Products</h4>
                                <ul>
                                    <li><a href="#">Managed Website</a></li>
                                    <li><a href="#">Manage Reputation</a></li>
                                    <li><a href="#">Power Tools</a></li>
                                    <li><a href="#">Marketing Service</a></li>
                                </ul>
                            </div>
                            <div className="col-lg-2 col-md-6 single-footer-widget">
                                <h4>Quick Links</h4>
                                <ul>
                                    <li><a href="#">Jobs</a></li>
                                    <li><a href="#">Brand Assets</a></li>
                                    <li><a href="#">Investor Relations</a></li>
                                    <li><a href="#">Terms of Service</a></li>
                                </ul>
                            </div>
                            <div className="col-lg-2 col-md-6 single-footer-widget">
                                <h4>Features</h4>
                                <ul>
                                    <li><a href="#">Jobs</a></li>
                                    <li><a href="#">Brand Assets</a></li>
                                    <li><a href="#">Investor Relations</a></li>
                                    <li><a href="#">Terms of Service</a></li>
                                </ul>
                            </div>
                            <div className="col-lg-2 col-md-6 single-footer-widget">
                                <h4>Resources</h4>
                                <ul>
                                    <li><a href="#">Guides</a></li>
                                    <li><a href="#">Research</a></li>
                                    <li><a href="#">Experts</a></li>
                                    <li><a href="#">Agencies</a></li>
                                </ul>
                            </div>
                            <div className="col-lg-4 col-md-6 single-footer-widget">
                                <h4>Newsletter</h4>
                                <p>You can trust us. we only send promo offers,</p>
                                <div className="form-wrap" id="mc_embed_signup">
                                    <form target="_blank"
                                          action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&id=92a4423d01"
                                          method="get" className="form-inline">
                                        <input className="form-control" name="EMAIL" placeholder="Your Email Address"
                                               onfocus="this.placeholder = ''"
                                               onblur="this.placeholder = 'Your Email Address '" required type="email"/>
                                        <button className="click-btn btn btn-default">Subscribe</button>
                                        <div style={{position: 'absolute', left: '-5000px'}}>
                                            <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabIndex={-1}
                                                   defaultValue type="text"/>
                                        </div>
                                        <div className="info"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="footer-bottom row align-items-center">
                            <p className="footer-text m-0 col-lg-8 col-md-12">{/* Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. */}
                                Copyright © All rights reserved | This template is made with <i
                                    className="fa fa-heart-o" aria-hidden="true"/> by <a href="https://colorlib.com"
                                                                                         target="_blank">Colorlib</a>
                                {/* Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. */}
                            </p>
                            <div className="col-lg-4 col-md-12 footer-social">
                                <a href="#"><i className="fa fa-facebook"/></a>
                                <a href="#"><i className="fa fa-twitter"/></a>
                                <a href="#"><i className="fa fa-dribbble"/></a>
                                <a href="#"><i className="fa fa-behance"/></a>
                            </div>
                        </div>
                    </div>
                </footer>
                {/*================ End footer Area  =================*/}
                {/* Optional JavaScript */}
                {/* jQuery first, then Popper.js, then Bootstrap JS */}
            </div>
        );
    }

}

export default Cart;