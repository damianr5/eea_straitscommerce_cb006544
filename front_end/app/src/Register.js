import React, { Component } from 'react';

class Register extends Component{

    constructor(props){
        super(props);
        this.state = {
            firstName: '',
            lastName:'',
            username:'',
            email:'',
            password:'',
            dob:'',
        };
     this.changeHandler = this.changeHandler.bind(this);
     this.userRegistrationHandler = this.userRegistrationHandler.bind(this);

    }

    changeHandler = event=>
        this.setState({[event.target.name]: event.target.value});

    userRegistrationHandler(event){
        event.preventDefault();
        const data = new FormData(event.target);
        fetch("http://localhost:8080/users", {
            method: "POST",
            body: data
        });
        window.location = "Login";
    }

    render(){
        return (
            <div>
                {/* Required meta tags */}
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                <link rel="icon" href="img/favicon.png" type="image/png" />
                <title>Straits commerce</title>

                {/*================Header Menu Area =================*/}
                <header className="header_area">
                    <div className="top_menu">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-7">
                                    <div className="float-left">
                                        <p>Phone: +94 777 769 670</p>
                                        <p>email: sgt.info@straitsglobal.com</p>
                                    </div>
                                </div>
                                <div className="col-lg-5">
                                    <div className="float-right">
                                        <ul className="right_side">
                                            <li>
                                                <a href="contact.html">
                                                    Contact Us
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="main_menu_register">
                        <div className="container">
                            <nav className="navbar navbar-expand-lg navbar-light w-100">
                                {/* Brand and toggle get grouped for better mobile display */}
                                <a className="navbar-brand logo_h" href="/">
                                    <img className="logo" src={require("./img/logo.png")} alt="" />
                                </a>
                            </nav>
                        </div>
                    </div>

                    {/*================Signup Area =================*/}
                    <div className="main-signup">
                        <section className="signup">
                            {/*<img src={require("./img/signup-bg.jpg")} alt=""/>*/}
                            <div className="container-signup">
                                <div className="signup-content">
                                    <form method="POST" id="signup-form" className="signup-form" onSubmit={this.userRegistrationHandler}>
                                        <h2 className="form-title">Create account</h2>
                                        <div className="form-group">
                                            <input type="text" className="form-input" name="firstName" id="first_name" placeholder="First name" value={this.state.firstName} onChange={this.changeHandler}/>
                                        </div>
                                        <div className="form-group">
                                            <input type="text" className="form-input" name="lastName" id="last_name" placeholder="Last name" value={this.state.lastName} onChange={this.changeHandler}/>
                                        </div>
                                        <div className="form-group">
                                            <input type="text" className="form-input" name="username" id="username" placeholder="Username" value={this.state.username} onChange={this.changeHandler}/>
                                        </div>
                                        <div className="form-group">
                                            <input type="date" className="form-input" name="dob" id="dob" placeholder="Date of Birth" value={this.state.dob} onChange={this.changeHandler}/>
                                        </div>
                                        <div className="form-group">
                                            <input type="email" className="form-input" name="email" id="email" placeholder="Your Email" value={this.state.email} onChange={this.changeHandler}/>
                                        </div>
                                        <div className="form-group">
                                            <input type="password" className="form-input" name="password" id="password" placeholder="Password" value={this.state.password} onChange={this.changeHandler}/>
                                            <span toggle="#password" className="zmdi zmdi-eye field-icon toggle-password" />
                                        </div>
                                        <div className="form-group">
                                            <input type="checkbox" name="agree-term" id="agree-term" className="agree-term" />
                                            <label htmlFor="agree-term" className="label-agree-term"><span><span /></span>I agree all statements in  <a href="#" className="term-service">Terms of service</a></label>
                                        </div>
                                        {/*<div className="form-group">*/}
                                        {/*    <input type="submit"  />*/}
                                        {/*</div>*/}
                                        <button type="submit" value="submit" className="form-submit">Submit
                                        </button>
                                    </form>
                                    <p className="loginhere">
                                        Have already an account ? <a href="/login" className="loginhere-link">Login here</a>
                                    </p>
                                </div>
                            </div>
                        </section>
                    </div>
                </header>

            </div>
        );
    }
}

export default Register;