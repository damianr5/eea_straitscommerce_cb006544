import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Register from './Register';
import Login from './Login';
import DetailedProduct from './DetailedProduct';
import UserProfile from './UserProfile';
import Cart from './Cart';
import Checkout from './Checkout'

class App extends Component {
  render() {
    return (
        <Router>
          <Switch>
            <Route path='/' exact={true} component={Home}/>
            <Route path='/register' exact={true} component={Register}/>
              <Route path='/login' exact={true} component={Login}/>
              <Route path='/singleproduct' exact={true} component={DetailedProduct}/>
              <Route path='/profile' exact={true} component={UserProfile}/>
              <Route path='/cart' exact={true} component={Cart}/>
              <Route path='/checkout' exact={true} component={Checkout}/>
          </Switch>
        </Router>
    )
  }
}

export default App;