import React, {Component} from 'react';

class UserProfile extends Component {

    userDetails = {
        first_name: '',
        last_name: '',
        username: '',
        email: '',
        password: '',
        dob: ''
    };

    constructor(props){
        super(props);
        this.state = {item: this.userDetails};
        this.state = {userId: localStorage.getItem("userId")};
        this.state = {};
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.updateUserInfo = this.updateUserInfo.bind(this);
    }

    componentDidMount() {
        if(localStorage.getItem("userId")!== 'new'){
            fetch(`http://localhost:8080/users/${localStorage.getItem('userId')}`)
                .then(response => {
                    console.log(response)
                    return response.json();
                }).then(data => {
                    this.setState({item: data});
            })
        }
    }

    onChangeHandler(event){
        let item = this.state.item;
        item[event.target.name] = event.target.value;
        this.setState({item});
    }

    updateUserInfo(event){
        event.preventDefault();
        const data = new FormData(event.target);
        fetch(`http://localhost:8080/users/${localStorage.getItem('userId')}`,{
            method: "PUT",
            body: data
        });
        window.location.href = "/profile";
    }

    render() {

        const user = this.state.item;
        return (
            <div>
                {/* Required meta tags */}
                <meta charSet="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
                <link rel="icon" href="img/favicon.png" type="image/png"/>
                <title>Straits commerce</title>

                {/*================Header Menu Area =================*/}
                <header className="header_area">
                    <div className="top_menu">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-7">
                                    <div className="float-left">
                                        <p>Phone: +94 777 769 670</p>
                                        <p>email: sgt.info@straitsglobal.com</p>
                                    </div>
                                </div>
                                <div className="col-lg-5">
                                    <div className="float-right">
                                        <ul className="right_side">
                                            <li>
                                                <a href="contact.html">
                                                    Contact Us
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="main_menu_register">
                        <div className="container">
                            <nav className="navbar navbar-expand-lg navbar-light w-100">
                                {/* Brand and toggle get grouped for better mobile display */}
                                <a className="navbar-brand logo_h" href="/">
                                    <img className="logo" src={require("./img/logo.png")} alt=""/>
                                </a>
                            </nav>
                        </div>
                    </div>

                    {/*================Signup Area =================*/}
                    {user !== undefined ? (
                        <div className="main-signup">
                            <section className="signup">
                                <div className="container-signup">
                                    <div className="signup-content">
                                        <form onSubmit={this.updateUserInfo} className="signup-form">
                                            <h2 className="form-title">Account Details</h2>
                                            <div className="form-group">
                                                <input type="text" className="form-input" name="first_name"
                                                       id="first_name"
                                                       placeholder="First name" value={user.first_name} onChange={this.onChangeHandler}/>
                                            </div>
                                            <div className="form-group">
                                                <input type="text" className="form-input" name="last_name" id="last_name"
                                                       placeholder="Last name" value={user.last_name} onChange={this.onChangeHandler}/>
                                            </div>
                                            <div className="form-group">
                                                <input type="text" className="form-input" name="username" id="username"
                                                       placeholder="Username" value={user.username} onChange={this.onChangeHandler}/>
                                            </div>
                                            <div className="form-group">
                                                <input type="date" className="form-input" name="dob" id="dob"
                                                       placeholder="Date of Birth" value={user.dob} onChange={this.onChangeHandler}/>
                                            </div>
                                            <div className="form-group">
                                                <input type="email" className="form-input" name="email" id="email"
                                                       placeholder="Your Email" value={user.email} onChange={this.onChangeHandler}/>
                                            </div>
                                            <div className="form-group">
                                                <input type="password" className="form-input" name="password"
                                                       id="password"
                                                       placeholder="Password" value={user.password} onChange={this.onChangeHandler}/>
                                            </div>
                                            <button type="submit" value="update" className="form-submit">Update
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    ) : ("")
                    }
                </header>

            </div>
        );
    }
}

export default UserProfile;